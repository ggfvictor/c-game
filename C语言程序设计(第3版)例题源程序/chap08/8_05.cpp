/* 【例8-5】输入n个正整数，将它们从小到大排序后输出，要求使用冒泡排序算法。*/

/*  冒泡排序算法 */
#include <stdio.h>
void swap2 (int *, int *);
void bubble (int a[ ], int n);
int main(void)
{    
	int n, a[8];
	int i;

	printf("Enter n (n<=8): ");
	scanf("%d", &n);
	printf("Enter a[%d] : ",n);
	for (i=0; i<n;i++)
		scanf("%d",&a[i]);
	bubble(a,n);
	printf("After sorted, a[%d] = ", n);
	for (i=0; i<n; i++)
		printf("%3d",a[i]);

	return 0;
}
void bubble (int a[ ], int n)  		/*  n是数组a中待排序元素的数量 */
{
   int  i, j, t;
   for( i = 1; i < n; i++ )		        /*  外部循环  */
      for (j = 0; j < n-i; j++ )	    /*  内部循环  */
        if (a[j] > a[j+1]){		    	/*  比较两个元素的大小  */
          t=a[j]; a[j]=a[j+1]; a[j+1]=t;	/*  如果前一个元素大，则交换 */
		 }
}
