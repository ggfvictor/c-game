/* 【例6-1】单词加密解析。输入一个英文单词（由六个小写英文字母组成），按照下列过程将该单词加密：先将英文单词中的小写字母转换为对应的大写字母，再将该大写字母的ASCII码对10整除后取其余数，从而得到一个六位整数密码。*/

#include <stdio.h> 
int main(void)
{
	int i;
	char ch_lower, ch_upper;
    
	for(i = 1; i <= 6; i++)
	{
	    scanf("%c", &ch_lower);
		if(ch_lower >= 'a' && ch_lower <= 'z')			
		    ch_upper = ch_lower - 'a' + 'A';		/*将小写字母转换为大写字母*/
		printf("%c->%c->%d\n", ch_lower, ch_upper, ch_upper%10);
	}

    return 0;
}
